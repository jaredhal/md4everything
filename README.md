# md4everything

## a static site generator using markdown for everything
 
### An attempt at a very unopinionated static site generator, for turning markdown into everything else.

In an attempt to be unopinionated, all it needs is markdown files. Make known the source directory, point at an output folder, and you'll get the files generated for your specified formats.

If you want to use templates, they'll match the structure of your content files, i.e. ${sourcfile}.md is mapped to ${sourcefile}.html for html, ${sourcefile}.gmi for gemini, etc.

For pages where no such mapping exists, they'll be rendered with a base template if it exists (named "base.html" for html, "base.gmi" for gemini, etc). Otherwise, it'll be pure markdown conversion to the target format.

Templating language is Jinja2, see (here)[https://jinja.palletsprojects.com/en/3.1.x/templates/] for a users guide.

Feeds are folders wherein the contents are shared to the templates for rendering, and to feed generators for making web feeds a la RSS or Atom.

Pull requests, comments, and critiques are more than welcome, I made this tool to meet my needs and hopefully the needs of others. 

I'm always open to adding another format to support.