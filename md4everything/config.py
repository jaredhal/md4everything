class Config:

    def __init__(self,
                 source='src',
                 output='out',
                 templates='templates',
                 static='static',
                 feeds=['posts'],
                 page_formats=['html','gemini'],
                 feed_formats=['rss'],
                 menus=[]
                 ):
        self.source = source
        self.output = output
        self.templates = templates
        self.static = static
        self.feeds = feeds
        self.page_formats = page_formats
        self.feed_formats = feed_formats
        self.base_template = "base"
        self.menus = menus
    def set_key(self, key, value):
        setattr(self, key, value)