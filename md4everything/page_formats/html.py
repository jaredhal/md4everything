import os
import re

import mistune

class html(mistune.HTMLRenderer):
    NAME = 'html'
    EXT = 'html'

    def __init__(self):
        super(mistune.HTMLRenderer, self).__init__()
        self._escape = False

    def text(self, text):
        return text

    def emphasis(self, text):
        return f'<em>{text}</em>'

    def strong(self, text):
        return f'<strong>{text}</strong>'

    def codespan(self, text):
        return f'<code>{text}</code>'

    def linebreak(self):
        return '<br/>\n'

    def link(self, link, text=None, title=None):
        # convert relative .md links to html
        r = re.compile(r'(^[\S]+)(.md)$')
        m = r.match(link)
        if m is not None and len(m.groups()) > 1:
            link = m.group(1) + '.' + self.EXT

        if text is None:
            text = link

        s = '<a href="' + link + '"'
        if title:
            s += ' title="' + title + '"'
        return s + '>' + (text or link) + '</a>'

    def image(self, src, alt="", title=None):
        return f'<img src={src} title={title} alt={alt} />'
    
    def paragraph(self, text):
        return f'<p>{text}</p>'

    def heading(self, text, level):
        el = f'h{level}'
        return f'<{el}>{text}</{el}>'

    def thematic_break(self):
        return '<hr/>\n'

    def block_text(self, text):
        return f'{text}\n'
    
    def block_code(self, text, info=None):
        return f'<code>{text}</code>'

    def inline_html(self, text):
        return text

    def block_quote(self, text):
        return f'<blockquote>{text}</blockquote>'

    def list(self, text, ordered, level, start=None):
        if ordered:
            html = "<ol"
            if start is not None:
                html += ' start="' + str(start) + '"'
            return html + '>\n' + text + '</ol>\n'
        else:
            return '<ul>\n' + text + '</ul>\n'
    
    def list_item(self, text, level):
        return f'<li>{text}</li>\n'