import mistune
import re


class gemini(mistune.HTMLRenderer):
    NAME = 'gemini'
    EXT = 'gmi'

    # inline types
    def __init__(self):
        super(mistune.HTMLRenderer, self).__init__()
        self._escape = False

    def text(self, text):
        return text

    def emphasis(self, text):
        return f'*{text}*'

    def strong(self, text):
        return self.emphasis(text)

    def codespan(self, text):
        return text

    def linebreak(self):
        return "\n"

    # single line blocks
    def link(self, link, text=None, title=None):
        # convert relative .md links to gmi
        r = re.compile(r'(^[\S]+)(.md)$')
        m = r.match(link)
        if m is not None and len(m.groups()) > 1:
            link = m.group(1) + '.' + self.EXT
        d = [text, title]
        desc = ' '.join(filter(None, d))
        return f"=> {link} {desc}"

    def image(self, src, alt="", title=None):
        return self.link(src, alt, title)

    def paragraph(self, text):
        return f'{text}\n'

    def heading(self, text, level):
        level = min(level, 3)
        return ('#' * level) + f' {text}\n'

    def thematic_break(self):
        return '-----\n'

    # multiline blocks
    def block_text(self, text):
        return f'{text}\n'

    def block_code(self, text, info=None):
        return f'```{text}```'

    def inline_html(self, html):
        return self.block_code(text)

    def block_quote(self, text):
        quotes = ""
        for line in text:
            quotes = quotes + f"> {line}"
        return quotes

    def block_html(self, text):
        return self.block_text(text)

    def block_error(self, text):
        return self.block_text(text)

    def list(self, text, ordered, level, start=None):
        return text

    def list_item(self, text, level):
        return f'* {text}'

    def table(self, text):
        return text

    def table_head(self, text):
        return text

    def table_body(self, text):
        return text

    def table_row(self, text):
        return text

    def table_cell(self, text, align=None, is_head=False):
        return text

    def footnote_ref(self, key, index):
        return f'{key}[{index}]'

    def footnotes(self, text):
        return text

    def footnote_item(self, text, key, index):
        return f'{key}[{index}]:{text}'

    def finalize(self, data):
        return ''.join(data)
