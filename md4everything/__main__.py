import argparse

from . import config
from . import generator


def parse_args():
    parser = argparse .ArgumentParser(
        description="static site converter that uses markdown to generate various web formats & feeds")
    parser.add_argument('-c', '--config', metavar='CONFIG_FILE',
                        help='the config file to use, which should be valid python. Use --help or README.md for config options')
    parser.add_argument('-s', '--source', metavar="SOURCE_DIR",
                        help='the directory storing the markdown files')
    parser.add_argument('-o', '--output', metavar="OUT_DIR",
                        help='the output directory to store the generated content')
    parser.add_argument('-t', '--templates', metavar="TEMPLATE_DIR",
                        help='the directory storing the template files')
    parser.add_argument('-a', '--static', metavar="STATIC_DIR",
                        help='the directory storing the static assets, if different than the templates or source directory')
    parser.add_argument('-f', '--feeds', metavar='FEED_DIRS', nargs='?',
                        help='the directories which will be parsed into feeds, i.e. blog posts, articles, etc')
    parser.add_argument('-m', '--menus', metavar='MENU_DIRS', nargs='?',
                        help='the directories which will be parsed into menus and referenced anywhere in the site')
    parser.add_argument('-ff', '--feedformats', metavar='FEED_FORMATS', nargs='?', help='feed formats to publish, seperated by commas; options include rss,atom')
    parser.add_argument('-pf', '--pageformats', metavar='PAGE_FORMATS', nargs='?', help='page formats to publish, seperated by commans; options include html,gemini,gopher')
    return parser.parse_args()


if __name__ == "__main__":
    kwargs = vars(parse_args())
    conf = config.Config()
    for key in kwargs:
        if kwargs[key] is not None:
            conf.set_key(key, kwargs[key])

    g = generator.Generator(conf)
    g.generate()
