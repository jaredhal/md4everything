import os
import importlib
import shutil

from jinja2 import Environment, FileSystemLoader, TemplateNotFound
import frontmatter
import mistune

from .feed_formats import *
from .page_formats import *


class Generator:
    ''' generate site context and pass it to all format
    '''

    def __init__(self, conf):
        self.source = os.path.abspath(conf.source)
        self.output = os.path.abspath(conf.output)
        d = []
        if conf.templates is not None:
            self.templates = os.path.abspath(conf.templates)
            for root, dirs, files in os.walk(self.templates):
                d.append(root)
            self.base_template = conf.base_template
        if conf.static is not None:
            self.static_source_dir = os.path.abspath(conf.static)
            d.append(self.static_source_dir)
            static_name = os.path.basename(self.static_source_dir)
            self.static_out_dir = os.path.join(self.output, static_name)
        self.env = Environment(loader=FileSystemLoader(d))
        self.page_formats = []
        self.feed_formats = []
        self.feeds = conf.feeds
        self.menus = conf.menus
        self.feeds_context = {}
        self.menus_context = {}
        for f in conf.page_formats:
            mod_name = ".page_formats." + f
            pyobj = importlib.import_module(mod_name, "md4everything")
            module = getattr(pyobj, f)
            md = mistune.create_markdown(renderer=module())
            md.NAME = md.renderer.NAME
            md.EXT = md.renderer.EXT
            self.page_formats.append(md)

        # TODO: feed formats

    def write_file(self, content, filepath):
        parentdir = os.path.dirname(filepath)
        if not os.path.exists(parentdir):
            os.mkdirs(parentdir)
        with open(filepath, "w") as w:
            w.write(content)
            w.close()

    def render_template(self, format, basename, text, metadata, static_relpath):
        rendered_text = format(text)
        try:
            t = self.env.get_template(basename + '.' + format.EXT)
        except TemplateNotFound as e:
            try:
                t = self.env.get_template(
                    self.base_template + '.' + format.EXT)
            except TemplateNotFound as eb:
                return rendered_text
        # menus context should be in page metadata
        print('metadata for page', basename)
        print('    ', metadata)
        return t.render(body=rendered_text, static=static_relpath, **metadata, **self.feeds_context)

    def out_filepath(self, src_abs_filepath, fmt):
        filename = os.path.basename(src_abs_filepath)
        basename = os.path.splitext(filename)[0]
        new_filename = basename + '.' + fmt.EXT
        parent_dir = os.path.dirname(src_abs_filepath)
        rel_filepath = os.path.relpath(parent_dir, start=self.source)
        out_path = os.path.join(self.output, fmt.NAME, rel_filepath, new_filename)
        return out_path

    def make_format_dirs(self):
        format_dirs = {}
        for frm in self.page_formats:
            frm_dir = os.path.join(self.output, frm.NAME)
            if not os.path.exists(frm_dir):
                os.makedirs(frm_dir)
            format_dirs[frm.NAME] = frm_dir
        return format_dirs

    def make_page_context(self, filepath):
        print("making context for page:", filepath)
        metadata = frontmatter.load(filepath).metadata
        print("    ")
        rel_url = os.path.relpath(filepath, start=self.source)
        print("    rel_url:", rel_url)
        metadata['relative_base_url'] = os.path.splitext(rel_url)[0]
        print("    relative_base_url", metadata['relative_base_url'])
        for menu_name in self.menus_context:
            menu_items = []
            for i in self.menus_context[menu_name]:
                this_item = {}
                this_item['basename'] = i['basename']
                item_abs_path = i['abs_src_path']
                item_rel_url = os.path.relpath(item_abs_path, start=os.path.dirname(filepath))
                print('for menu item', i['basename'])
                print('    item_abs_path', item_abs_path)
                print('    item_rel_url', item_rel_url)
                print('    from', filepath)
                for fmt in self.page_formats:
                    fmt_url = os.path.splitext(item_rel_url)[0] + '.' + fmt.EXT
                    this_item[fmt.NAME + '_url'] = fmt_url
                    print("    item_format_url", fmt_url)
                menu_items.append(this_item)
            metadata[menu_name] = menu_items
        return metadata

    def generate_feeds_context(self, root, files):
        print('generating feed for ', root)
        feed_pages = []
        for f in files:
            filepath = os.path.join(root, f)
            metadata = self.make_page_context(filepath)
            for frm in self.page_formats:
                metadata[frm.NAME +
                         "_url"] = metadata['relative_base_url'] + '.' + frm.EXT
            feed_pages.append(metadata)
        return feed_pages

    def generate_menus_context(self, root, files):
        print('generating menu context for', root, 'with', len(files), 'items')
        basename = os.path.basename(root)
        if basename in self.menus:
            menu_items = []
            for f in files:
                this_item = {}
                print('for menu item', f)
                basename = os.path.splitext(os.path.basename(f))[0]
                abs_src_path = os.path.abspath(os.path.join(root, f))
                this_item['basename'] = basename
                this_item['abs_src_path'] = abs_src_path
                menu_items.append(this_item)
                print('    basename:', basename)
                print('    abs_src_path', abs_src_path)
        return menu_items

    def generate(self):
        format_dirs = self.make_format_dirs()
        if self.static_out_dir:
            shutil.copytree(self.static_source_dir, self.static_out_dir, dirs_exist_ok=True)
        if len(self.feeds) > 0 or len(self.menus) > 0:
            self.feeds_context = {}
            for root, dirs, files in os.walk(self.source):
                root_basename = os.path.basename(root)
                print('found root_basename', root_basename, "for root", root)
                if root_basename in self.menus:
                    self.menus_context[root_basename] = self.generate_menus_context(
                        root, files)
                if root_basename in self.feeds:
                    self.feeds_context[root_basename] = self.generate_feeds_context(
                        root, files)

        for root, dirs, files in os.walk(self.source):
            menus = {}
            for frm in self.page_formats:
                root_relpath = os.path.relpath(root, start=self.source)
                out_abspath = os.path.join(
                    self.output, frm.NAME, root_relpath)
                static_relpath = os.path.relpath(
                    self.static_out_dir, start=out_abspath)
                frm_relpath = os.path.join(
                    format_dirs[frm.NAME], root_relpath)
                if not os.path.exists(frm_relpath):
                    os.makedirs(frm_relpath)
                for f in files:
                    filepath = os.path.join(root, f)
                    basename = os.path.splitext(os.path.basename(f))[0]
                    context = self.make_page_context(filepath)
                    context[frm.NAME +
                            '_url'] = context['relative_base_url'] + '.' + frm.EXT
                    md_file = frontmatter.load(os.path.join(root, f))
                    content = self.render_template(
                        frm, basename, md_file.content, context, static_relpath)
                    filename = basename + '.' + frm.EXT
                    write_path = os.path.join(frm_relpath, filename)
                    self.write_file(content, write_path)
